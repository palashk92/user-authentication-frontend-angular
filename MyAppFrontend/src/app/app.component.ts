import { Component } from '@angular/core';
import {HttpClient, HttpHeaders, HttpInterceptor} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private http: HttpClient) {}
  // http: HttpClient;
  url: string;
  text1 = 'Press Button 1';
  text2 = 'Press Button 2';
  headers: HttpHeaders;
  onButton1() {
    // this.text1 = 'Button 1 Pressed';
    // this.headers.append('realm', 'Realm1');
    this.http.get('http://localhost:8080/', {
      headers: {'realm': 'Realm1'},
      responseType: 'text',
      observe: 'response'
    }).subscribe(res => this.url = res.url);
    console.log(this.url);
    // window.location.href = this.url;
    window.open(this.url, '_self');
  }

  onButton2() {
    this.http.get('http://localhost:8080/login', {
      headers: {'realm': 'Realm2'},
      responseType: 'text'
    }).subscribe(res => this.url = res);
    console.log(this.url);
    this.text2 = 'Button 2 Pressed';
  }
}
